import $ from 'jquery'
import 'jquery.fancybox/source/jquery.fancybox.pack.js'
import 'jquery.fancybox/source/jquery.fancybox.css'

export default () => {

	$('#phone, #phone1, #phone2, #phone3, #phone4').mask('+7(999) 999-99-99')

	//мобильное меню
	$('.icon-menu-mobile').on('click', function() {
		$(this).toggleClass('open')
		$('.icon-menu-mobile').hasClass('open') ? ($('.menu').stop(!0, !0).slideDown()) : ($('.menu').slideUp())
	})
	$('.menu li a').on('click', function() {
		$('.icon-menu-mobile').removeClass('open')
	})

	//фокус полей
	$('input,textarea').focus(function () {
		$(this).data('placeholder', $(this).attr('placeholder'))
		$(this).attr('placeholder', '')
	})
	$('input,textarea').blur(function () {
		$(this).attr('placeholder', $(this).data('placeholder'))
	})

	//valid
	$(function(){
		$('.modal-call-form').validate({
			success: 'valid',
			submitHandler: function() {
				jQuery.fancybox.close();
				window.location.reload();
			},
			messages: {
				name4: {
					required: 'Введите Ваше имя'
				},
				phone4: {
					required: 'Укажите Ваш номер телефона'
				},
				check4: {
					required: '<p>Согласие на обработку персональных данных</p>'
				},
				
			}
		});
	});

	$(function(){
		$('.modal-inline').fancybox({
			margin: 0,
			padding: 20,
			maxWidth: 400,
			autoScale: true,
			transitionIn: 'none',
			transitionOut: 'none',
			type: 'inline',
			helpers: {
				overlay: {
					locked: false
				}
			}
		});
	});

	
}