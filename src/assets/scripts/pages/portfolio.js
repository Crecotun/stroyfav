import $ from 'jquery'
import header from '../components/header.js'
import button_up from '../components/button_up.js'
import 'slick-carousel/slick/slick.js'
import 'slick-carousel/slick/slick.css'
import 'vendor/jquery.maskedinput'
import 'vendor/jquery.validate'

$(document).ready(() => {
	header();
	button_up();

	$('#phone, #phone1, #phone2').mask('+7(999) 999-99-99')

	//validate
	$(function(){
		$('.question-form').validate({
			success: 'valid',
			submitHandler: function() {
			},
			messages: {
				phone: {
					required: 'Укажите Ваш номер телефона'
				},
				check: {
					required: '<p>Согласие на обработку персональных данных</p>'
				},
				
			}
		});
	});

	//slick
	$('.slider-image').slick({
		infinite: false,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows : true,
		dots: false,
		autoplay: false,
		prevArrow: '<button class="slick-prev slick-arrow" type="button"></button>', 
		nextArrow: '<button class="slick-next slick-arrow" type="button"></button>',
	});
	//tabs
	$('.tabs-portfolio').each(function () {
		var $active, $content, $links = $(this).find('a');
		$active = $($links.filter('[href="' + location.hash + '"]')[0] || $links[0]);
		$active.addClass('active');
		$content = $($active[0].hash);
		$links.not($active).each(function () {
			$(this.hash).hide();
		});
		$(this).on('click', 'a', function (e) {
			e.preventDefault();
			$active.removeClass('active').delay(1000);
			$content.hide();
			$active = $(this);
			$content = $(this.hash);
			$active.addClass('active');
			$content.show(10, function () {
				$('.slider-show, .slider-image').slick('setPosition');
			});
		});
	});

})