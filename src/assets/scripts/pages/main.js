import $ from 'jquery'
import header from '../components/header.js'
import button_up from '../components/button_up.js'
import 'tooltipster/dist/js/tooltipster.bundle.js'
import 'tooltipster/dist/css/tooltipster.bundle.css'
import 'slick-carousel/slick/slick.js'
import 'slick-carousel/slick/slick.css'
import 'vendor/jquery.maskedinput'
import 'vendor/jquery.validate'

$(document).ready(() => {
	header();
	button_up();

	//phone
	$('#phone, #phone1, #phone2, #phone3').mask('+7(999) 999-99-99')

	//validate
	$(function(){
		$('.question-form').validate({
			success: 'valid',
			submitHandler: function() {
			},
			messages: {
				phone: {
					required: 'Укажите Ваш номер телефона'
				},
				check: {
					required: '<p>Согласие на обработку персональных данных</p>'
				},
				
			}
		});
	});
	$(function(){
		$('.consultation-form').validate({
			success: 'valid',
			submitHandler: function() {
			},
			messages: {
				name1: {
					required: 'Введите Ваше имя'
				},
				phone1: {
					required: 'Укажите Ваш номер телефона'
				},
				check1: {
					required: '<p>Согласие на обработку персональных данных</p>'
				},
				
			}
		});
	});
	$(function(){
		$('.offer-form').validate({
			success: 'valid',
			submitHandler: function() {
			},
			messages: {
				name2: {
					required: 'Введите Ваше имя'
				},
				phone2: {
					required: 'Укажите Ваш номер телефона'
				},
				check2: {
					required: '<p>Согласие на обработку персональных данных</p>'
				},
				
			}
		});
	});



	//file input
	$(function() {
		$('#file-upload').on('change', function() {
			$('.form__file-value').html( $(this).val().replace (/\\/g, '/').split ('/').pop ())
		});
	});


	//всплывашки

	$('.tooltip-html').tooltipster({
		delay: 100,
		maxWidth: 500,
		speed: 300,
		contentCloning: true,
		interactive: true,
		theme: 'tooltipster-main',
		animation: 'grow',
		trigger: 'click',
		side: 'right',
		functionPosition: function(instance, helper, position){
			position.coord.top += +40;
			position.coord.left += -10;
			return position;
		}
	});
	if($(window).width() <= 1230) {
		$('.tooltip-html').tooltipster('destroy');
		$('.tooltip-html').tooltipster({
			delay: 100,
			maxWidth: 500,
			speed: 300,
			contentCloning: true,
			interactive: true,
			theme: 'tooltipster-main',
			animation: 'grow',
			trigger: 'click',
			side: 'top',
		});
	}
		
	//slick
	$('.reviews-slider').slick({
		infinite: false,
		speed: 300,
		slidesToShow: 2,
		slidesToScroll: 1,
		arrows : false,
		dots: true,
		autoplay: false,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
		]
	});
	$('.brands-slider').slick({
		infinite: true,
		speed: 300,
		slidesToShow: 5,
		slidesToScroll: 1,
		arrows : true,
		dots: false,
		autoplay: true,
		prevArrow: '<button class="slick-prev slick-arrow" type="button"></button>', 
		nextArrow: '<button class="slick-next slick-arrow" type="button"></button>',
		responsive: [
			{
				breakpoint: 991,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 768,
				settings: {
					infinite: true,
					dots: true,
					arrows: false,
					autoplay: true,
					slidesToShow: 4,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 568,
				settings: {
					infinite: true,
					dots: true,
					arrows: false,
					slidesToShow: 3,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 461,
				settings: {
					infinite: true,
					dots: true,
					arrows: false,
					slidesToShow: 2,
					slidesToScroll: 1
				}
			},
		]
	});
	$('.slider-show').slick({
		infinite: false,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows : false,
		dots: true,
		autoplay: false,
		responsive: [
			{
				breakpoint: 1230,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 568,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
		]
	});
	$('.slider-image').slick({
		infinite: false,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows : true,
		dots: false,
		autoplay: false,
		prevArrow: '<button class="slick-prev slick-arrow" type="button"></button>', 
		nextArrow: '<button class="slick-next slick-arrow" type="button"></button>',
	});

	//tabs
	$('.tabs-success, .top-goods__tabs').each(function () {
		var $active, $content, $links = $(this).find('a');
		$active = $($links.filter('[href="' + location.hash + '"]')[0] || $links[0]);
		$active.addClass('active');
		$content = $($active[0].hash);
		$links.not($active).each(function () {
			$(this.hash).hide();
		});
		$(this).on('click', 'a', function (e) {
			e.preventDefault();
			$active.removeClass('active').delay(1000);
			$content.hide();
			$active = $(this);
			$content = $(this.hash);
			$active.addClass('active');
			$content.show(10, function () {
				$('.slider-show, .slider-image').slick('setPosition');
			});
		});
	});


})
