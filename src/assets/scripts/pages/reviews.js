import $ from 'jquery'
import header from '../components/header.js'
import button_up from '../components/button_up.js'
import 'vendor/jquery.maskedinput'
import 'vendor/jquery.validate'

$(document).ready(() => {
	header();
	button_up();

	$('#phone, #phone1, #phone2').mask('+7(999) 999-99-99')

	//validate
	$(function(){
		$('.question-form').validate({
			success: 'valid',
			submitHandler: function() {
			},
			messages: {
				phone: {
					required: 'Укажите Ваш номер телефона'
				},
				check: {
					required: '<p>Согласие на обработку персональных данных</p>'
				},
				
			}
		});
	});

	$('.modal-iframe').fancybox({
		type:'swf',
		allowfullscreen: 'true'
	});

})