import $ from 'jquery'
import header from '../components/header.js'
import button_up from '../components/button_up.js'
import 'vendor/jquery.maskedinput'
import 'vendor/jquery.validate'

$(document).ready(() => {
	header();
	button_up();

	$('#phone, #phone1, #phone2').mask('+7(999) 999-99-99')

	//validate
	$(function(){
		$('.question-form').validate({
			success: 'valid',
			submitHandler: function() {
			},
			messages: {
				phone: {
					required: 'Укажите Ваш номер телефона'
				},
				check: {
					required: '<p>Согласие на обработку персональных данных</p>'
				},
				
			}
		});
	});

	//карта яндекс

	ymaps.ready(init);

	function init() {
		// Создание карты.
		var myMap = new ymaps.Map('map', {
			center: [46.318020, 39.382483],
			zoom: 16,
			controls: [
				'zoomControl', // Ползунок масштаба
				'rulerControl', // Линейка
				'routeButtonControl', // Панель маршрутизации
				'trafficControl', // Пробки
				'typeSelector', // Переключатель слоев карты
				'fullscreenControl', // Полноэкранный режим
				// Поисковая строка
				new ymaps.control.SearchControl({
					options: {
						// вид - поисковая строка
						size: 'large',
						// Включим возможность искать не только топонимы, но и организации.
						provider: 'yandex#search'
					}
				})
			]
		});
		// Добавление метки
		var myPlacemark = new ymaps.Placemark([46.318020, 39.382483], {
			// Хинт показывается при наведении мышкой на иконку метки.
			hintContent: 'СТРОЙФАВОРИТ',
			// Балун откроется при клике по метке.
			balloonContent: 'Окна, двери, ворота для частного дома, предприятия или завода'
		});
		myMap.geoObjects.add(myPlacemark);
		
		//отключаем зум колёсиком мышки
		myMap.behaviors.disable('scrollZoom');
		//на мобильных устройствах... (проверяем по userAgent браузера)
		if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
			//... отключаем перетаскивание карты
			myMap.behaviors.disable('drag');
		}


	}

	

})