import $ from 'jquery'
import header from '../components/header.js'
import button_up from '../components/button_up.js'
import 'slick-carousel/slick/slick.js'
import 'slick-carousel/slick/slick.css'
import 'vendor/jquery.maskedinput'
import 'vendor/jquery.validate'

$(document).ready(() => {
	header();
	button_up();

	$('#phone, #phone1, #phone2, #phone3').mask('+7(999) 999-99-99')

	//validate
	$(function(){
		$('.question-form').validate({
			success: 'valid',
			submitHandler: function() {
			},
			messages: {
				phone: {
					required: 'Укажите Ваш номер телефона'
				},
				check: {
					required: '<p>Согласие на обработку персональных данных</p>'
				},
				
			}
		});
	});
	$(function(){
		$('.questions-form').validate({
			success: 'valid',
			submitHandler: function() {
			},
			messages: {
				name3: {
					required: 'Введите Ваше имя'
				},
				phone3: {
					required: 'Укажите Ваш номер телефона'
				},
				check3: {
					required: '<p>Согласие на обработку персональных данных</p>'
				},
				textarea3: {
					required: 'Ведите сообщение'
				},
				
			}
		});
	});

	//slick
	$('.brands-slider').slick({
		infinite: true,
		speed: 300,
		slidesToShow: 5,
		slidesToScroll: 1,
		arrows : true,
		dots: false,
		autoplay: true,
		prevArrow: '<button class="slick-prev slick-arrow" type="button"></button>', 
		nextArrow: '<button class="slick-next slick-arrow" type="button"></button>',
		responsive: [
			{
				breakpoint: 991,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 768,
				settings: {
					infinite: true,
					dots: true,
					arrows: false,
					autoplay: true,
					slidesToShow: 4,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 568,
				settings: {
					infinite: true,
					dots: true,
					arrows: false,
					slidesToShow: 3,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 461,
				settings: {
					infinite: true,
					dots: true,
					arrows: false,
					slidesToShow: 2,
					slidesToScroll: 1
				}
			},
		]
	});

})